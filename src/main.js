import Vue from 'vue'
import App from './App.vue'

import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import VueCookies from 'vue-cookies'

import Routes from './routes'

import axios from 'axios';

Vue.use(VueResource)
Vue.use(VueRouter)
Vue.use(VueCookies)

VueCookies.config(Infinity)

axios.defaults.baseURL = '//' + window.location.hostname + '/api';
// axios.defaults.baseURL = 'https://myband.info/api';

var isAuth = $cookies.isKey('logged_in')
// var isAuth = true;

const router = new VueRouter({
  routes: Routes,
  mode: 'history'
});

router.beforeEach((to, from, next) => {
  if (to.name !== 'login' && !isAuth) {
    next('/login');
  } else if (to.name === 'login' && isAuth) {
      next('/');
  } else {
    next();
  }
})

Vue.filter('humanize', date => {
    if (date == null) return '';

    date = new Date(date);
    return date.toLocaleString('en-gb', { day: "numeric", month: "long", year: "numeric" });
});

Vue.filter('toLower', str => {
  return str.toLowerCase();
});

Vue.filter('leadingZero', num => {
  return (num < 10 ? '0' : '') + num;
});

Vue.filter('checkDateFormat', s => {
  var regex = new RegExp(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/);
  return regex.test(s);
});

Vue.filter('reformatDate', s => {
  return (new Date(s)).toISOString();
});

export const common = new Vue({
  data: {
    bandId: $cookies.get('band_id'),
    oneDay: 60 * 60 * 24 * 1000,
    months: [
      { num: '01', name: 'Jan', fullName: 'January'},
      { num: '02', name: 'Feb', fullName: 'February'},
      { num: '03', name: 'Mar', fullName: 'March'},
      { num: '04', name: 'Apr', fullName: 'April'},
      { num: '05', name: 'May', fullName: 'May'},
      { num: '06', name: 'Jun', fullName: 'June'},
      { num: '07', name: 'Jul', fullName: 'Jule'},
      { num: '08', name: 'Aug', fullName: 'August'},
      { num: '09', name: 'Sep', fullName: 'September'},
      { num: '10', name: 'Oct', fullName: 'October'},
      { num: '11', name: 'Nov', fullName: 'November'},
      { num: '12', name: 'Dec', fullName: 'December'}
    ],
    bands: [],
    members: [],
    roles: [],
    songs: [],
    origins: [],
    songStatuses: [],
    songNameVariations: [],
    locations: [],
    venues: [],
    gigs: [],
    lists: [],
    baseUrl: axios.defaults.baseURL,
    isAuth: $cookies.isKey('logged_in')
    // isAuth: true
  },
  methods: {
    toSlug(s) {
      return s.replace(/[^a-zA-Z\d\s\_\-]/g, '').replace(/(\s|\_|\-)+/g, '-').toLowerCase();
    }
  }
});

new Vue({
  el: '#app',
  render: h => h(App),
  router
})
