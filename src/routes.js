import login from './components/login.vue';

import home from './components/home.vue';
import members from './components/members/members.vue';
import roles from './components/roles/roles.vue';
import songs from './components/songs/songs.vue';
import origins from './components/origins/origins.vue';
import songStatuses from './components/song_statuses/song_statuses.vue';
import songNameVariations from './components/song_name_variations/song_name_variations.vue';
import gigs from './components/gigs/gigs.vue';
import locations from './components/locations/locations.vue';
import venues from './components/venues/venues.vue';
import lists from './components/lists/lists.vue';

export default [
    
    { path: '/', component: home, name: 'home' },

    { path: '/login', component: login, name: 'login' },

    { path: '/members', component: members, name: 'members' },

    { path: '/members/:sortStatus/:sortRole/:selectedComponent', component: members, name: 'sortedMembers' },
    
    { path: '/roles', component: roles, name: 'roles' },

    { path: '/origins', component: origins, name: 'origins' },

    { path: '/songs', component: songs, name: 'songs' },

    { path: '/songs/:sortOrigin/:sortStatus', component: songs, name: 'sortedSongs' },

    { path: '/song_statuses', component: songStatuses, name: 'songStatuses' },

    { path: '/song_name_variations', component: songNameVariations, name: 'songNameVariations' },

    { path: '/gigs', component: gigs, name: 'gigs' },

    { path: '/gigs/:sortVenue/:sortLocation', component: gigs, name: 'sortedGigs' },

    { path: '/locations', component: locations, name: 'locations' },

    { path: '/venues', component: venues, name: 'venues' },

    { path: '/venues/:sortStatus/:sortLocation', component: venues, name: 'sortedVenues' },

    { path: '/lists', component: lists, name: 'lists' },

    { path: '*', redirect: '/'}
]