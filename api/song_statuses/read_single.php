<?php

require_once '../headers.php';
require_once '../db.php';

$ss2bId = isset($_GET['ss2b_id']) ? $_GET['ss2b_id'] : die();

$query = "SELECT
		ss.id AS status_id,
		ss.name,
		ss.slug,
		ss.bg_color,
		ss.fg_color,
        ss.locked,
		ss2b.id AS ss2b_id
	FROM song_statuses AS ss
	JOIN song_statuses_to_bands AS ss2b
        ON ss2b.status_id = ss.id
    WHERE ss2b.id = :ss2b_id
    LIMIT 0,1";

$stmt = $conn->prepare($query);
$stmt->bindParam(':ss2b_id', $ss2bId);
$stmt->execute();

$arr = array();

$status = $stmt->fetch(PDO::FETCH_ASSOC);
extract($status);

$item  = array(
    'id' => (int)$status_id,
    'ss2bId' => (int)$ss2bId,
    'name' => $name,
    'slug' => $slug,
    'bgColor' => $bg_color,
    'fgColor' => $fg_color,
    'locked' => $locked == 1
);

echo json_encode($item);
