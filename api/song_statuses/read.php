<?php

require_once '../headers.php';
require_once '../db.php';

$byCurrentBand = isset($_GET['by_current_band']) ? $_GET['by_current_band'] === 'true' : false;

$query = "SELECT
		ss.id AS status_id,
		ss.name,
		ss.slug,
		ss.bg_color,
		ss.fg_color,
        ss.locked,
		ss2b.id AS ss2b_id
	FROM song_statuses AS ss
	JOIN song_statuses_to_bands AS ss2b
        ON ss2b.status_id = ss.id ";

if ($byCurrentBand) {
	$query .= " WHERE ss2b.band_id = :band_id ";
} else {
	$query .= " WHERE NOT EXISTS (
		SELECT *
		FROM song_statuses_to_bands
		WHERE song_statuses_to_bands.band_id = :band_id
		AND song_statuses_to_bands.status_id = ss.id) ";
}

$stmt = $conn->prepare($query);
$stmt->bindParam(':band_id', $bandId);
$stmt->execute();

$arr = array();

while ($status = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($status);
    
	$item  = array(
        'id' => (int)$status_id,
        'ss2bId' => (int)$ss2b_id,
        'name' => $name,
        'slug' => $slug,
        'bgColor' => $bg_color,
        'fgColor' => $fg_color,
        'locked' => $locked == 1
	);

	array_push($arr, $item);
}

echo json_encode($arr);
