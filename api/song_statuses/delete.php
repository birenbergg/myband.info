<?php

require_once '../headers.php';
require_once '../db.php';

$statusId = json_decode(file_get_contents("php://input"));

// Check how many bands status has

$checkQuery = "SELECT COUNT(*)
	FROM  song_statuses_to_bands
    WHERE status_id = :status_id";

$checkStmt = $conn->prepare($checkQuery);
$checkStmt->bindParam(':status_id', $statusId);
$checkStmt->execute();

$numOfBandsStatusHas = (int)$checkStmt->fetchColumn();

// Delete role
$deleteQuery = "";

if ($numOfBandsStatusHas == 1) {
    $deleteQuery = "DELETE FROM song_statuses WHERE id = :id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $statusId);
} else if ($numOfBandsStatusHas > 1) {
    $deleteQuery = "DELETE FROM song_statuses_to_bands WHERE status_id = :id AND band_id = :band_id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $statusId);
    $deleteStmt->bindParam(':band_id', $bandId);
}

$deleteStmt->execute();

echo "OK";
