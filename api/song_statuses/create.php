<?php

require_once '../headers.php';
require_once '../db.php';

$status = json_decode(file_get_contents("php://input"));

// Add Variations:

// Create New
if ($status->id == null) {

    $query = "INSERT INTO song_statuses 
        SET
            name = :name,
            slug = :slug,
            bg_color = :bg_color,
            fg_color = :fg_color";

    $stmt = $conn->prepare($query);

    $stmt->bindParam(':name', $status->name);
    $stmt->bindParam(':slug', $status->slug);
    $stmt->bindParam(':bg_color', $status->bgColor);
    $stmt->bindParam(':fg_color', $status->fgColor);

    $stmt->execute();

    $newStatusId = $conn->lastInsertId();

// Add existing
} else {
    $newStatusId = $status->id;
}

// Add Relation to the Band
$s2bQuery = "INSERT INTO song_statuses_to_bands
    SET
        band_id = :band_id,
        status_id = :status_id";
        
$s2bStmt = $conn->prepare($s2bQuery);

$s2bStmt->bindParam(':band_id', $bandId);
$s2bStmt->bindParam(':status_id', $newStatusId);

$s2bStmt->execute();

$newS2bId = $conn->lastInsertId();

echo $newS2bId;
