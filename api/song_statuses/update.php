<?php

require_once '../headers.php';
require_once '../db.php';

$status = json_decode(file_get_contents("php://input"));

$query = "UPDATE song_statuses
			SET
                name = :name,
                slug = :slug,
                bg_color = :bg_color,
                fg_color = :fg_color
            WHERE id = :id";
        
$stmt = $conn->prepare($query);

$stmt->bindParam(':id', $status->id);
$stmt->bindParam(':name', $status->name);
$stmt->bindParam(':slug', $status->slug);
$stmt->bindParam(':bg_color', $status->bgColor);
$stmt->bindParam(':fg_color', $status->fgColor);

$stmt->execute();
