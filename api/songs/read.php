<?php

require_once '../headers.php';
require_once '../db.php';

$bring_gigs = isset($_GET['gigs']) ? $_GET['gigs'] === 'true' : false;
$active_only = isset($_GET['active_only']) ? $_GET['active_only'] === 'true' : false;
$byCurrentBand = isset($_GET['by_current_band']) ? $_GET['by_current_band'] === 'true' : false;

$songsQuery = "SELECT
	s.id,
	s.name AS song_name,
	s.length,
	s.origin_id,
	s2b.id AS s2b_id,
	s2b.status_id,
	ss.name AS status,
	ss.bg_color,
	ss.fg_color,
	o.name AS origin_name,
	(CASE
		WHEN s.name LIKE 'A %' THEN SUBSTR(s.name, 3)
		WHEN s.name LIKE 'An %' THEN SUBSTR(s.name, 4)
		WHEN s.name LIKE 'The %' THEN SUBSTR(s.name, 5)
		ELSE s.name END
	) AS sort_name
	FROM songs AS s
	JOIN songs_to_bands AS s2b
		ON s2b.song_id = s.id
	JOIN origins AS o
		ON o.id = s.origin_id
	JOIN song_statuses AS ss
		ON ss.id = s2b.status_id ";

if ($byCurrentBand) {
	$songsQuery .= " WHERE s2b.band_id = :band_id ";
} else {
	$songsQuery .= " WHERE NOT EXISTS (
		SELECT *
		FROM songs_to_bands
		WHERE songs_to_bands.band_id = :band_id
		AND songs_to_bands.song_id = s.id) ";
}
    // .
    $songsQuery .= ($active_only ? " AND s.status_id = 1 " : "");
    // .
    $songsQuery .= " ORDER BY sort_name";



$songStmt = $conn->prepare($songsQuery);
$songStmt->bindParam(':band_id', $bandId);
$songStmt->execute();

$songArr = array();

while ($song = $songStmt->fetch(PDO::FETCH_ASSOC)) {
	extract($song);

	if ($bring_gigs === true) {
		// *** Get Gigs
		$gigArr = array();

		$gigQuery = "SELECT g.date, g2b.name AS gig_name
			FROM songs_to_g2b AS s2g
				JOIN songs AS s
					ON s.id = s2g.song_id
				JOIN gigs AS g
					ON g.id = s2g.g2b_id
				JOIN gigs_to_bands AS g2b
					ON g.id = g2b.gig_id
				WHERE s.id = :id
				AND g.date < CURDATE()";

		$gigStmt = $conn->prepare($gigQuery);
		$gigStmt->bindParam(':id', $id); // song_id
		$gigStmt->execute();

		while($gig = $gigStmt->fetch(PDO::FETCH_ASSOC)) {
			extract($gig);

			$gigItem  = array(
				'date' => $date,
				'name' => $gig_name
			);
	
			array_push($gigArr, $gigItem);
		}
		// *** End of Get Gigs
	}

	$songItem  = array(
		'id' => (int)$id,
		's2bId' => (int)$s2b_id,
		'name' => $song_name,
		'sortName' => $sort_name,
		'totalLength' => $length != null ? (int)$length : null,
		'minutes' => $length != null ? floor($length / 60) : null,
		'seconds' => $length != null ? $length - floor($length / 60) * 60 : null,
		'statusId' => (int)$status_id,
		'status' => $status,
		'originId' => (int)$origin_id,
		'originName' => $origin_name,
		'bgColor' => $bg_color,
		'fgColor' => $fg_color
	);

	if ($bring_gigs === true && count($gigArr)) {
		$songItem['gigs'] = $gigArr;
	}

	array_push($songArr, $songItem);
}

echo json_encode($songArr);
