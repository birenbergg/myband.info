<?php

require_once '../headers.php';
require_once '../db.php';

$song = json_decode(file_get_contents("php://input"));

// Update Song

$query = "UPDATE songs AS s
        JOIN songs_to_bands AS s2b
            ON s2b.song_id = s.id
        SET
            s.name = :name,
            s.length = :length,
            s.origin_id = :origin_id,
            s2b.status_id = :status_id

            WHERE s.id = :song_id
            AND s2b.id = :s2b_id";
        
$stmt = $conn->prepare($query);

$stmt->bindParam(':song_id', $song->id);
$stmt->bindParam(':s2b_id', $song->s2bId);
$stmt->bindParam(':name', $song->name);
$stmt->bindParam(':length', $length = ($song->minutes == 0 && $song->seconds == 0) ? null : $song->minutes * 60 + $song->seconds);
$stmt->bindParam(':origin_id', $song->originId);
$stmt->bindParam(':status_id', $song->statusId);

$stmt->execute();
