<?php

require_once '../headers.php';
require_once '../db.php';

$song = json_decode(file_get_contents("php://input"));

if ($song->id == null) {
    // Add Song
    $songQuery = "INSERT INTO songs
                SET
                    name = :name,
                    length = :length,
                    origin_id = :origin_id";
    
    $songStmt = $conn->prepare($songQuery);
    
    $songStmt->bindParam(':name', $song->name);
    $songStmt->bindParam(':length', $length = ($song->minutes == 0 && $song->seconds == 0) ? null : $song->minutes * 60 + $song->seconds);
    $songStmt->bindParam(':origin_id', $song->originId);
    
    $songStmt->execute();
    
    $newSongId = $conn->lastInsertId();
} else {
    $newSongId = $song->id;
}

// Add Relation to the Band
$s2bQuery = "INSERT INTO songs_to_bands
    SET
        band_id = :band_id,
        song_id = :song_id,
        status_id = :status_id";
        
$s2bStmt = $conn->prepare($s2bQuery);

$s2bStmt->bindParam(':band_id', $bandId);
$s2bStmt->bindParam(':song_id', $newSongId);
$s2bStmt->bindParam(':status_id', $song->statusId);

$s2bStmt->execute();

$newS2bId = $conn->lastInsertId();

// Check If Song's Origin Is Related to Band
$originCheckQuery = "SELECT count(*)
    FROM origins_to_bands
    WHERE origin_id = :origin_id
    AND band_id = :band_id";

$originCheckStmt = $conn->prepare($originCheckQuery);
$originCheckStmt->bindParam(':origin_id', $song->originId);
$originCheckStmt->bindParam(':band_id', $bandId);

$originCheckStmt->execute();

$originsNum = (int)$originCheckStmt->fetchColumn();

// Add Origin Relation to the Band
if ($originsNum == 0) {
    $addOriginQuery = "INSERT INTO origins_to_bands
        SET
            band_id = :band_id,
            origin_id = :origin_id";
            
    $addOriginStmt = $conn->prepare($addOriginQuery);

    $addOriginStmt->bindParam(':band_id', $bandId);
    $addOriginStmt->bindParam(':origin_id', $song->originId);

    $addOriginStmt->execute();
}

echo $newS2bId;
