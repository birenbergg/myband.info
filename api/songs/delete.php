<?php

require_once '../headers.php';
require_once '../db.php';

$songId = json_decode(file_get_contents("php://input"));

$checkQuery = "SELECT COUNT(*)
	FROM  songs_to_bands
    WHERE song_id = :song_id";

$checkStmt = $conn->prepare($checkQuery);
$checkStmt->bindParam(':song_id', $songId);
$checkStmt->execute();

$numOfBandsSongHas = (int)$checkStmt->fetchColumn();

$deleteQuery = "";

if ($numOfBandsSongHas == 1) {
    $deleteQuery = "DELETE FROM songs WHERE id = :id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $songId);
} else if ($numOfBandsSongHas > 1) {
    $deleteQuery = "DELETE FROM songs_to_bands WHERE song_id = :id AND band_id = :band_id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $songId);
    $deleteStmt->bindParam(':band_id', $bandId);
}

$deleteStmt->execute();

echo "OK";
