<?php

require_once '../headers.php';
require_once '../db.php';

$snv2bId = isset($_GET['snv2b_id']) ? $_GET['snv2b_id'] : die();

$query = "SELECT
		snv.id AS snv_id,
		snv.variation,
		snv2b.id AS snv2b_id
	FROM song_name_variations AS snv
	JOIN song_name_variations_to_bands AS snv2b
        ON snv2b.song_name_variation_id = snv.id
    WHERE snv2b.id = :snv2b_id
    LIMIT 0,1";

$stmt = $conn->prepare($query);
$stmt->bindParam(':snv2b_id', $snv2bId);
$stmt->execute();

$arr = array();

$var = $stmt->fetch(PDO::FETCH_ASSOC);
extract($var);

$item  = array(
    'id' => (int)$snv_id,
    'snv2bId' => (int)$snv2bId,
    'variation' => $variation
);

echo json_encode($item);
