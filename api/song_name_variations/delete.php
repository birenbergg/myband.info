<?php

require_once '../headers.php';
require_once '../db.php';

$variationId = json_decode(file_get_contents("php://input"));

// Check how many bands variation has

$checkQuery = "SELECT COUNT(*)
	FROM  song_name_variations_to_bands
    WHERE song_name_variation_id = :song_name_variation_id";

$checkStmt = $conn->prepare($checkQuery);
$checkStmt->bindParam(':song_name_variation_id', $variationId);
$checkStmt->execute();

$numOfBandsVariationHas = (int)$checkStmt->fetchColumn();

// Delete role
$deleteQuery = "";

if ($numOfBandsVariationHas == 1) {
    $deleteQuery = "DELETE FROM song_name_variations WHERE id = :id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $variationId);
} else if ($numOfBandsVariationHas > 1) {
    $deleteQuery = "DELETE FROM song_name_variations_to_bands WHERE song_name_variation_id = :id AND band_id = :band_id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $variationId);
    $deleteStmt->bindParam(':band_id', $bandId);
}

$deleteStmt->execute();

echo "OK";
