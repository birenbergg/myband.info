<?php

require_once '../headers.php';
require_once '../db.php';

$var = json_decode(file_get_contents("php://input"));

$query = "UPDATE song_name_variations
			SET
                variation = :variation
            WHERE id = :id";
        
$stmt = $conn->prepare($query);

$stmt->bindParam(':id', $var->id);
$stmt->bindParam(':variation', $var->variation);

$stmt->execute();
