<?php

require_once '../headers.php';
require_once '../db.php';

$var = json_decode(file_get_contents("php://input"));

// Add Variations:

// Create New
if ($var->id == null) {

    $query = "INSERT INTO song_name_variations SET variation = :variation";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(':variation', $var->variation);
    $stmt->execute();

    $newVarId = $conn->lastInsertId();

// Add existing
} else {
    $newVarId = $var->id;
}

// Add Relation to the Band
$v2bQuery = "INSERT INTO song_name_variations_to_bands
    SET
        band_id = :band_id,
        song_name_variation_id = :song_name_variation_id";
        
$v2bStmt = $conn->prepare($v2bQuery);

$v2bStmt->bindParam(':band_id', $bandId);
$v2bStmt->bindParam(':song_name_variation_id', $newVarId);

$v2bStmt->execute();

$newV2bId = $conn->lastInsertId();

echo $newV2bId;
