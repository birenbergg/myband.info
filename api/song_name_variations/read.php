<?php

require_once '../headers.php';
require_once '../db.php';

$byCurrentBand = isset($_GET['by_current_band']) ? $_GET['by_current_band'] === 'true' : false;

$query = "SELECT
		snv.id AS snv_id,
		snv.variation,
		snv2b.id AS snv2b_id
	FROM song_name_variations AS snv
	JOIN song_name_variations_to_bands AS snv2b
		ON snv2b.song_name_variation_id = snv.id ";


if ($byCurrentBand) {
	$query .= " WHERE snv2b.band_id = :band_id ";
} else {
	$query .= " WHERE NOT EXISTS (
		SELECT *
		FROM song_name_variations_to_bands
		WHERE song_name_variations_to_bands.band_id = :band_id
		AND song_name_variations_to_bands.song_name_variation_id = snv.id) ";
}

$stmt = $conn->prepare($query);
$stmt->bindParam(':band_id', $bandId);
$stmt->execute();

$arr = array();

while ($var = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($var);
    
	$item  = array(
		'id' => (int)$snv_id,
		'snv2bId' => (int)$snv2b_id,
        'variation' => $variation
	);

	array_push($arr, $item);
}

echo json_encode($arr);
