<?php

require_once '../headers.php';
require_once '../db.php';

$memberId = json_decode(file_get_contents("php://input"));

$checkQuery = "SELECT COUNT(*)
	FROM  members_to_bands
    WHERE member_id = :member_id";

$checkStmt = $conn->prepare($checkQuery);
$checkStmt->bindParam(':member_id', $memberId);
$checkStmt->execute();

$numOfBandsMemberIsIn = (int)$checkStmt->fetchColumn();

$deleteQuery = "";

if ($numOfBandsMemberIsIn == 1) {
    $deleteQuery = "DELETE FROM members WHERE id = :id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $memberId);
} else if ($numOfBandsMemberIsIn > 1) {
    $deleteQuery = "DELETE FROM members_to_bands WHERE member_id = :id AND band_id = :band_id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $memberId);
    $deleteStmt->bindParam(':band_id', $bandId);
}

$deleteStmt->execute();
