<?php

require_once '../headers.php';
require_once '../db.php';

$member = json_decode(file_get_contents("php://input"));

if ($member->id == null) {
    // Add Member
    $memberQuery = "INSERT INTO members
                SET
                    first_name = :first_name,
                    last_name = :last_name,
                    fb_id = :fb_id";
            
    $memberStmt = $conn->prepare($memberQuery);

    $memberStmt->bindParam(':first_name', $member->firstName);
    $memberStmt->bindParam(':last_name', $member->lastName);
    $memberStmt->bindParam(':fb_id', $member->fbId);

    $memberStmt->execute();

    $newMemberId = $conn->lastInsertId();
} else {
    $newMemberId = $member->id;
}

// Add Relation to the Band
$m2bQuery = "INSERT INTO members_to_bands
			SET
                member_id = :member_id,
                band_id = :band_id,
                is_guest = :is_guest";
        
$m2bStmt = $conn->prepare($m2bQuery);

$m2bStmt->bindParam(':member_id', $newMemberId);
$m2bStmt->bindParam(':band_id', $bandId);
$m2bStmt->bindParam(':is_guest', $isGuest = $member->isGuest ? 1 : 0);

$m2bStmt->execute();

$m2bId = $conn->lastInsertId();

// Add Roles
if ($member->roles != null) {
    $roleIndex = 1;
    foreach ($member->roles as $role) {
        if (!$role->delete) {
            $roleQuery = "INSERT INTO m2b_to_r2b
                SET
                    m2b_id = :m2b_id,
                    r2b_id = :r2b_id,
                    `index` = :index,
                    `started` = :role_started,
                    ended = :ended,
                    level = :level";
            
            $roleStmt = $conn->prepare($roleQuery);

            $roleStmt->bindParam(':m2b_id', $m2bId);
            $roleStmt->bindParam(':r2b_id', $role->r2bId);
            $roleStmt->bindParam(':index', $roleIndex);
            $roleStmt->bindParam(':role_started', $role->started);
            $roleStmt->bindParam(':ended', $ended = $role->ended == '' ? null : $role->ended);
            $roleStmt->bindParam(':level', $role->level);

            $roleStmt->execute();

            $roleIndex++;
        }
    }
}
// End: Add Roles

echo $m2bId;
