<?php

require_once '../headers.php';
require_once '../db.php';

$member = json_decode(file_get_contents("php://input"));


// Update Member

$memQuery = "UPDATE members AS m
    JOIN members_to_bands AS m2b
        ON m2b.member_id = m.id
    SET
        m.first_name = :first_name,
        m.last_name = :last_name,
        m2b.is_guest = :is_guest,
        m.fb_id = :fb_id
    WHERE m.id = :mem_id
    AND m2b.id = :m2b_id";
        
$memStmt = $conn->prepare($memQuery);

$memStmt->bindParam(':mem_id', $member->id);
$memStmt->bindParam(':m2b_id', $member->m2bId);
$memStmt->bindParam(':first_name', $member->firstName);
$memStmt->bindParam(':last_name', $member->lastName);
$memStmt->bindParam(':is_guest', $isGuest = $member->isGuest ? 1 : 0);
$memStmt->bindParam(':fb_id', $fbId = $member->fbId == "" ? null : $member->fbId);

$memStmt->execute();


// Update Roles
// (first delete all roles)
$deleteRolesQuery = "DELETE FROM m2b_to_r2b
    WHERE
        m2b_id = :m2b_id";
            
$deleteRolesStmt = $conn->prepare($deleteRolesQuery);

$deleteRolesStmt->bindParam(':m2b_id', $member->m2bId);

$deleteRolesStmt->execute();

// then add the roles again
if ($member->roles != null) {
    $roleIndex = 1;
    foreach ($member->roles as $role) {
        if (!$role->delete) {
    
            $roleQuery = "INSERT INTO m2b_to_r2b
			SET
                m2b_id = :m2b_id,
                r2b_id = :r2b_id,
                `index` = :index,
                `started` = :started,
                ended = :ended,
                level = :level";
        
            $roleStmt = $conn->prepare($roleQuery);

            $roleStmt->bindParam(':m2b_id', $member->m2bId);
            $roleStmt->bindParam(':r2b_id', $role->r2bId);
            $roleStmt->bindParam(':index', $roleIndex);
            $roleStmt->bindParam(':started', $role->started);
            $roleStmt->bindParam(':ended', $ended = $role->ended == '' ? null : $role->ended);
            $roleStmt->bindParam(':level', $role->level);

            $roleStmt->execute();

            $roleIndex++;
        }
    }
}
