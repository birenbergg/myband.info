<?php

function getRoleStartDates($roleArr) {
	return $roleArr['started'];
}

function getRoleEndDates($roleArr) {
	return $roleArr['ended'];
}

function getRoleNames($roleArr) {
	return $roleArr['name'];
}

require_once '../headers.php';
require_once '../db.php';

$m2b_id = isset($_GET['m2b_id']) ? $_GET['m2b_id'] : die();

$memQuery = "SELECT
    m.id AS member_id,
    m.first_name,
    m.last_name,
    m.fb_id,
	m2b.is_guest
    FROM members AS m
    JOIN members_to_bands AS m2b
        ON m2b.member_id = m.id
    WHERE m2b.id = ?
    LIMIT 0,1";
        
$memStmt = $conn->prepare($memQuery);
$memStmt->bindParam(1, $m2b_id);
$memStmt->execute();

$mem = $memStmt->fetch(PDO::FETCH_ASSOC);
extract($mem);

// *** Get Roles
$roleArr = array();

$roleQuery = "SELECT
			r.id AS id,
			r.name,
			r.slug,
			r.color,
			r2b.id AS r2b_id,
			m2r.id AS linking_id,
			m2r.started,
			m2r.ended,
			m2r.index,
			m2r.level
		FROM roles AS r
			JOIN roles_to_bands AS r2b
				ON r2b.role_id = r.id
			JOIN m2b_to_r2b AS m2r
				ON m2r.r2b_id = r2b.id
			JOIN members_to_bands AS m2b
				ON m2b.id = m2r.m2b_id
			WHERE m2b.id = :m2b_id
			ORDER BY m2r.index";

$roleStmt = $conn->prepare($roleQuery);
$roleStmt->bindParam(':m2b_id', $m2b_id);
$roleStmt->execute();

while ($role = $roleStmt->fetch(PDO::FETCH_ASSOC)) {
    extract($role);

    $roleItem  = array(
        'id' => (int)$id,
        'linkingId' => (int)$linking_id,
        'r2bId' => (int)$r2b_id,
        'name' => $name,
        'slug' => $slug,
        'color' => $color,
        'started' => $started,
        'ended' => $ended,
        'index' => (int)$index,
        'level' => (int)$level,
        'delete' => false,
        'edit' => false,
        'added' => true
    );

    array_push($roleArr, $roleItem);
}
// *** End of Get Roles

$startedArray = array_map('getRoleStartDates', $roleArr);
$endedArray = array_map('getRoleEndDates', $roleArr);
$namesArray = array_map('getRoleNames', $roleArr);

$startDate = min($startedArray);
$endDate = in_array(null, $endedArray) ? null : max($endedArray);
$uniqueNames = array_unique($namesArray);

$memItem  = array(
    'id' => (int)$member_id,
    'm2bId' => (int)$m2b_id,
    'firstName' => $first_name,
    'lastName' => $last_name,
    'dateJoined' => $startDate,
    'dateLeft' => $endDate,
    'isCurrent' => $endDate == null && $is_guest == 0,
    'isGuest' => $is_guest == 1,
    'fbId' => $fb_id
);


$memItem['roles'] = $roleArr;
$memItem['uniqueRoles'] = $uniqueNames;

echo json_encode($memItem);
