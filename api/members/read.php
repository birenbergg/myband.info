<?php

function getRoleStartDates($roleArr) {
	return $roleArr['started'];
}

function getRoleEndDates($roleArr) {
	return $roleArr['ended'];
}

function getRoleNames($roleArr) {
	return $roleArr['name'];
}

require_once '../headers.php';
require_once '../db.php';

$byCurrentBand = isset($_GET['by_current_band']) ? $_GET['by_current_band'] === 'true' : false;

$memQuery = "SELECT
		m.id AS member_id,
		m.first_name,
		m.last_name,
		m.fb_id,
		m2b.is_guest,
		m2b.id AS m2b_id
	FROM members AS m
	JOIN members_to_bands AS m2b ON m.id = m2b.member_id ";
	
	if ($byCurrentBand) {
		$memQuery .= " WHERE m2b.band_id = :band_id ";
	} else {
		$memQuery .= " 
		 WHERE NOT EXISTS (
			SELECT *
			FROM members_to_bands
			WHERE members_to_bands.band_id = :band_id
			AND members_to_bands.member_id = m.id) ";
	}

$memStmt = $conn->prepare($memQuery);
$memStmt->bindParam(':band_id', $bandId);
$memStmt->execute();

$memArr = array();

while ($mem = $memStmt->fetch(PDO::FETCH_ASSOC)) {
	extract($mem);

	// *** Get Roles
	$roleArr = array();

	$roleQuery = "SELECT
			r.id AS id,
			r.name,
			r.slug,
			r.color,
			r2b.id AS r2b_id,
			m2r.id AS linking_id,
			m2r.started,
			m2r.started_year,
			m2r.started_season,
			m2r.started_month,
			m2r.started_day,
			m2r.ended,
			m2r.ended_year,
			m2r.ended_season,
			m2r.ended_month,
			m2r.ended_day,
			m2r.index,
			m2r.level
		FROM roles AS r
			JOIN roles_to_bands AS r2b
				ON r2b.role_id = r.id
			JOIN m2b_to_r2b AS m2r
				ON m2r.r2b_id = r2b.id
			JOIN members_to_bands AS m2b
				ON m2b.id = m2r.m2b_id
			WHERE m2b.id = :m2b_id
			ORDER BY m2r.index";

	$roleStmt = $conn->prepare($roleQuery);
	$roleStmt->bindParam(':m2b_id', $m2b_id);
	$roleStmt->execute();

	while ($role = $roleStmt->fetch(PDO::FETCH_ASSOC)) {
		extract($role);

		$roleItem  = array(
			'id' => (int)$id,
			'linkingId' => (int)$linking_id,
			'r2bId' => (int)$r2b_id,
			'name' => $name,
			'slug' => $slug,
			'color' => $color,
			'started' => $started,
			'startedYear' => $started_year == null ? null : (int)$started_year,
			'startedSeason' => $started_season == null ? null : (int)$started_season,
			'startedMonth' => $started_month == null ? null : (int)$started_month,
			'startedDay' => $started_day == null ? null : (int)$started_day,
			'ended' => $ended,
			'endedYear' => $ended_year == null ? null : (int)$ended_year,
			'endedSeason' => $ended_season == null ? null : (int)$ended_season,
			'endedMonth' => $ended_month == null ? null : (int)$ended_month,
			'endedDay' => $ended_day == null ? null : (int)$ended_day,
			'index' => (int)$index,
			'level' => (int)$level,
			'delete' => false,
            'edit' => false,
            'added' => true
		);

		array_push($roleArr, $roleItem);
	}
	// *** End of Get Roles

	$startedArray = array_map('getRoleStartDates', $roleArr);
	$endedArray = array_map('getRoleEndDates', $roleArr);
	$namesArray = array_map('getRoleNames', $roleArr);
	
	$startDate = min($startedArray);
	$endDate = in_array(null, $endedArray) ? null : max($endedArray);
	$uniqueNames = array_unique($namesArray);

	$memItem  = array(
		'id' => (int)$member_id,
		'm2bId' => (int)$m2b_id,
		'firstName' => $first_name,
		'lastName' => $last_name,
		'dateJoined' => $startDate,
        'dateLeft' => $endDate,
        'isCurrent' => $endDate == null && $is_guest == 0,
		'isGuest' => $is_guest == 1,
		'fbId' => $fb_id
    );
    
	$memItem['roles'] = $roleArr;
	$memItem['uniqueRoles'] = $uniqueNames;

	array_push($memArr, $memItem);
}

echo json_encode($memArr);
