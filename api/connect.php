<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: Content-Type, Cache-Control, X-Requested-With, X-CSRF-Token, Discourse-Visible, User-Api-Key, User-Api-Client-Id');

require_once 'config.php';

// Create connection
$conn = mysqli_connect($host, $username, $password, $db_name);
$conn->set_charset('utf8');

// Check connection
if (!$conn) {
    die("Connection failed: " . $conn->connect_error);
}
