<?php

require_once '../headers.php';
require_once '../db.php';

$originId = json_decode(file_get_contents("php://input"));

$checkQuery = "SELECT COUNT(*)
	FROM  origins_to_bands
    WHERE origin_id = :origin_id";

$checkStmt = $conn->prepare($checkQuery);
$checkStmt->bindParam(':origin_id', $originId);
$checkStmt->execute();

$numOfBandsOriginHas = (int)$checkStmt->fetchColumn();

$deleteQuery = "";

if ($numOfBandsOriginHas == 1) {
    $deleteQuery = "DELETE FROM origins WHERE id = :id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $originId);
} else if ($numOfBandsOriginHas > 1) {
    $deleteQuery = "DELETE FROM origins_to_bands WHERE origin_id = :id AND band_id = :band_id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $originId);
    $deleteStmt->bindParam(':band_id', $bandId);
}

$deleteStmt->execute();

echo "OK";

// $songQuery = "SELECT COUNT(*) FROM songs WHERE origin_id = :origin_id";

// $songStmt = $conn->prepare($songQuery);
// $songStmt->bindParam(':origin_id', $data->id);
// $songStmt->execute();

// $numOfSongs = $songStmt->fetchColumn();

// if ($numOfSongs > 0) {
//     echo "You have " . $numOfSongs . " song(s) related to this origin. Could not delete the origin. Delete these songs first or set another origin to these songs.";
//     return;
// }

// $query = "DELETE FROM origins WHERE id = :id";

// $stmt = $conn->prepare($query);
// $stmt->bindParam(':id', $data->id);
// $stmt->execute();

// echo "OK";
