<?php

require_once '../headers.php';
require_once '../db.php';

$origin = json_decode(file_get_contents("php://input"));

if ($origin->id == null) {

    // Add Origin
    $originQuery = "INSERT INTO origins
        SET
            name = :name,
            slug = :slug";
            
    $originStmt = $conn->prepare($originQuery);

    $originStmt->bindParam(':name', $origin->name);
    $originStmt->bindParam(':slug', $origin->slug);

    $originStmt->execute();

    $newOriginId = $conn->lastInsertId();
} else {
    $newOriginId = $origin->id;
}

// Add Relation to the Band
$o2bQuery = "INSERT INTO origins_to_bands
    SET
        band_id = :band_id,
        origin_id = :origin_id";
        
$o2bStmt = $conn->prepare($o2bQuery);

$o2bStmt->bindParam(':band_id', $bandId);
$o2bStmt->bindParam(':origin_id', $newOriginId);

$o2bStmt->execute();

$newo2bId = $conn->lastInsertId();

echo $newo2bId;

// $query = "INSERT INTO origins
//     SET
//         name = :name,
//         slug = :slug";
        
// $stmt = $conn->prepare($query);

// $stmt->bindParam(':name', $origin->name);
// $stmt->bindParam(':slug', $origin->slug);

// $stmt->execute();

// echo $conn->lastInsertId();
