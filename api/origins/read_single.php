<?php

require_once '../headers.php';
require_once '../db.php';

$o2bId = isset($_GET['o2b_id']) ? $_GET['o2b_id'] : die();

$query = "SELECT
        o.*,
	    o2b.id AS o2b_id,
        (CASE
            WHEN o.name LIKE 'A %' THEN SUBSTR(o.name, 3)
            WHEN o.name LIKE 'An %' THEN SUBSTR(o.name, 4)
            WHEN o.name LIKE 'The %' THEN SUBSTR(o.name, 5)
            ELSE o.name END
        ) AS sort_name
    FROM origins AS o
	JOIN origins_to_bands AS o2b
		ON o2b.origin_id = o.id
    WHERE o2b.id = :o2b_id
    LIMIT 0,1";

$stmt = $conn->prepare($query);
$stmt->bindParam(':o2b_id', $o2bId);
$stmt->execute();

$arr = array();

$origin = $stmt->fetch(PDO::FETCH_ASSOC);
extract($origin);

$item  = array(
    'id' => (int)$id,
    'o2bId' => (int)$o2b_id,
    'name' => $name,
    'sortName' => $sort_name,
    'slug' => $slug
);

echo json_encode($item);
