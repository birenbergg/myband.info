<?php

require_once '../headers.php';
require_once '../db.php';

$byCurrentBand = isset($_GET['by_current_band']) ? $_GET['by_current_band'] === 'true' : false;

$query = "SELECT
		o.*,
		o2b.id AS o2b_id,
		(CASE
			WHEN o.name LIKE 'A %' THEN SUBSTR(o.name, 3)
			WHEN o.name LIKE 'An %' THEN SUBSTR(o.name, 4)
			WHEN o.name LIKE 'The %' THEN SUBSTR(o.name, 5)
			ELSE o.name END
		) AS sort_name
	FROM origins AS o
	JOIN origins_to_bands AS o2b
		ON o2b.origin_id = o.id ";

if ($byCurrentBand) {
	$query .= " WHERE o2b.band_id = :band_id ";
} else {
	$query .= " WHERE NOT EXISTS (
		SELECT *
		FROM origins_to_bands
		WHERE origins_to_bands.band_id = :band_id
		AND origins_to_bands.origin_id = o.id) ";
}

$query .= " ORDER BY sort_name";

$stmt = $conn->prepare($query);
$stmt->bindParam(':band_id', $bandId);
$stmt->execute();

$arr = array();

while ($origin = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($origin);
    
	$item  = array(
		'id' => (int)$id,
		'o2bId' => (int)$o2b_id,
		'name' => $name,
		'sortName' => $sort_name,
		'slug' => $slug
	);

	array_push($arr, $item);
}

echo json_encode($arr);
