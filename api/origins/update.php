<?php

require_once '../headers.php';
require_once '../db.php';

$origin = json_decode(file_get_contents("php://input"));

$query = "UPDATE origins
		    SET
                name = :name,
                slug = :slug
            WHERE id = :id";
        
$stmt = $conn->prepare($query);

$stmt->bindParam(':id', $origin->id);
$stmt->bindParam(':name', $origin->name);
$stmt->bindParam(':slug', $origin->slug);

$stmt->execute();
