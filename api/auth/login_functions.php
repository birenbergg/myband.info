<?php

function logged_in()
{
	return isset($_COOKIE['admin_id']);
}

function confirm_query($result_set, $func_name, $query) {
    if (!$result_set) {
        die("Database query failed. Error in function: " . $func_name . "<br>" . $query);
    }
}

function find_admin($username)
{
	global $conn;

	$safe_username = mysqli_real_escape_string($conn, $username);

    $query  = "SELECT * FROM ";
	$query .= "admins ";
    $query .= "WHERE username = '{$safe_username}' ";
	$query .= "LIMIT 1";

	$admin_set = mysqli_query($conn, $query);
	
	confirm_query($admin_set, __FUNCTION__, $query);
    
	if ($admin = mysqli_fetch_assoc($admin_set))
	{
    	return $admin;
	}
	else
	{
		return null;
	}
}

function password_check($password, $existing_hash)
{
	$hash = crypt($password, $existing_hash);
	
	if ($hash == $existing_hash)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function change_password($old_password, $new_password, $existing_hash)
{
	$hash = crypt($old_password, $existing_hash);
	
	if ($hash == $existing_hash)
	{
		// change password
		/*
		$query  = "UPDATE admins ";
		$query .= "set ";
    	$query .= "WHERE username = '{$safe_username}' ";
		$query .= "LIMIT 1";
		*/

		// return true;
	}
	else
	{
		return false;
	}
}

function attempt_login($username, $password)
{
	$admin = find_admin($username);

	if ($admin)
	{
		if (password_check($password, $admin['password']))
		{
			setcookie('logged_in', true, time() + (100 * 365 * 24 * 60 * 60), "/"); // 100 * 365 * 24 * 60 * 60 = 100 years

			// return $admin;
			return 'success';
		}
		else
		{
			// return false;
			return 'password error';
		}
	}
	else
	{
		// return false;
		return 'user not found';
	}
}
