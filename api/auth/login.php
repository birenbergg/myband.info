<?php

    require_once '../connect.php';
	require_once 'login_functions.php';

	$creds = json_decode(file_get_contents("php://input"));

	echo attempt_login($creds->username, $creds->password);
	