<?php

require_once '../headers.php';
require_once '../db.php';

$byCurrentBand = isset($_GET['by_current_band']) ? $_GET['by_current_band'] === 'true' : false;

$query = "SELECT
		r.id AS role_id,
		r.name,
		r.slug,
		r.color,
		r2b.id AS r2b_id
	FROM roles AS r
	JOIN roles_to_bands AS r2b
		ON r.id = r2b.role_id ";

if ($byCurrentBand) {
	$query .= " WHERE r2b.band_id = :band_id ";
} else {
	$query .= " WHERE NOT EXISTS (
		SELECT *
		FROM roles_to_bands
		WHERE roles_to_bands.band_id = :band_id
		AND roles_to_bands.role_id = r.id) ";
}

$stmt = $conn->prepare($query);
$stmt->bindParam(':band_id', $bandId);
$stmt->execute();

$arr = array();

while ($role = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($role);
    
	$item  = array(
		'id' => (int)$role_id,
		'r2bId' => (int)$r2b_id,
		'name' => $name,
		'slug' => $slug,
		'color' => $color
	);

	array_push($arr, $item);
}

echo json_encode($arr);
