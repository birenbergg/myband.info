<?php

require_once '../headers.php';
require_once '../db.php';

$r2bId = isset($_GET['r2b_id']) ? $_GET['r2b_id'] : die();

$query = "SELECT
        r.id AS role_id,
		r.name,
		r.slug,
		r.color,
		r2b.id AS r2b_id
    FROM roles AS r
    JOIN roles_to_bands AS r2b
        ON r.id = r2b.role_id
    WHERE r2b.id = :r2b_id
    LIMIT 0,1";

$stmt = $conn->prepare($query);
$stmt->bindParam(':r2b_id', $r2bId);
$stmt->execute();

$arr = array();

$role = $stmt->fetch(PDO::FETCH_ASSOC);
extract($role);

$item  = array(
    'id' => (int)$role_id,
    'r2bId' => (int)$r2b_id,
    'name' => $name,
    'slug' => $slug,
    'color' => $color
);

echo json_encode($item);
