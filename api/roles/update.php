<?php

require_once '../headers.php';
require_once '../db.php';

$role = json_decode(file_get_contents("php://input"));

// Update Role

$roleQuery = "UPDATE roles AS r
    JOIN roles_to_bands AS r2b
        ON r2b.role_id = r.id
    SET
        r.name = :name,
        r.slug = :slug,
        r.color = :color
    WHERE r.id = :role_id
    AND r2b.id = :r2b_id";
        
$roleStmt = $conn->prepare($roleQuery);

$roleStmt->bindParam(':role_id', $role->id);
$roleStmt->bindParam(':r2b_id', $role->r2bId);
$roleStmt->bindParam(':name', $role->name);
$roleStmt->bindParam(':slug', $role->slug);
$roleStmt->bindParam(':color', $role->color);

$roleStmt->execute();
