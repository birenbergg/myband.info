<?php

require_once '../headers.php';
require_once '../db.php';

$role = json_decode(file_get_contents("php://input"));

// Add Role:

// Create New
if ($role->id == null) {

    $query = "INSERT INTO roles
                SET
                    name = :name,
                    slug = :slug,
                    color = :color";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(':name', $role->name);
    $stmt->bindParam(':slug', $role->slug);
    $stmt->bindParam(':color', $role->color);
    $stmt->execute();

    $newRoleId = $conn->lastInsertId();

// Add existing
} else {
    $newRoleId = $role->id;
}

// Add Relation to the Band
$r2bQuery = "INSERT INTO roles_to_bands
    SET
        band_id = :band_id,
        role_id = :role_id";
        
$r2bStmt = $conn->prepare($r2bQuery);

$r2bStmt->bindParam(':band_id', $bandId);
$r2bStmt->bindParam(':role_id', $newRoleId);

$r2bStmt->execute();

$newR2bId = $conn->lastInsertId();

echo $newR2bId;
