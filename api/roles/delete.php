<?php

require_once '../headers.php';
require_once '../db.php';

$role = json_decode(file_get_contents("php://input"));


// Check how many bands role has

$checkQuery = "SELECT COUNT(*)
	FROM  roles_to_bands
    WHERE role_id = :role_id";

$checkStmt = $conn->prepare($checkQuery);
$checkStmt->bindParam(':role_id', $role->id);
$checkStmt->execute();

$numOfBandsRoleHas = (int)$checkStmt->fetchColumn();


// Check how many members role has

$memQuery = "SELECT COUNT(*)
    FROM members AS m
    JOIN m2b_to_r2b AS m2r
        ON m2r.m2b_id = m.id
    WHERE m2r.r2b_id = :r2b_id";

$memStmt = $conn->prepare($memQuery);
$memStmt->bindParam(':r2b_id', $role->r2bId);
$memStmt->execute();

$numOfMembers = $memStmt->fetchColumn();

// if ($numOfMembers > 0) {
//     echo "You have " . $numOfMembers . " members(s) related to this role. Could not delete the role. Delete these members first or set another role to these members.";
//     return;
// }


// Delete role

$deleteQuery = "";

if ($numOfBandsRoleHas == 1) {
    $deleteQuery = "DELETE FROM roles WHERE id = :id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $role->id);
} else if ($numOfBandsRoleHas > 1) {
    $deleteQuery = "DELETE FROM roles_to_bands WHERE role_id = :id AND band_id = :band_id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $role->id);
    $deleteStmt->bindParam(':band_id', $bandId);
}

$deleteStmt->execute();

echo "OK";
