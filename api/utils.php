<?php

function reformatDate($date_str)
{
    $char = "";
    $old_arr = [3];
    $new_arr = [3];

    if (strpos($date_str, ".") > -1)
    {
        $char = ".";
    }
    else if (strpos($date_str, "/") > -1)
    {
        $char = "/";
    }
    else if (strpos($date_str, "-") > -1)
    {
        $char = "-";
    }

    $old_arr = explode($char, $date_str);

    error_log("strlen: " . strlen($old_arr[0]));
    error_log("old_arr[0]: " . $old_arr[0]);

    if (strlen($old_arr[2]) == 4)
    {
        $new_arr[0] = $old_arr[2];
        $new_arr[1] = $old_arr[1];
        $new_arr[2] = $old_arr[0];
    }
    else
    {
        $new_arr = $old_arr;
    }

    error_log("new_arr: " . count($new_arr));
    
    return implode("-", $new_arr);
}
