<?php

require_once '../headers.php';
require_once '../db.php';

$locationId = json_decode(file_get_contents("php://input"));

$checkQuery = "SELECT COUNT(*)
	FROM  locations_to_bands
    WHERE location_id = :location_id";

$checkStmt = $conn->prepare($checkQuery);
$checkStmt->bindParam(':location_id', $locationId);
$checkStmt->execute();

$numOfBandsLocationHas = (int)$checkStmt->fetchColumn();

$deleteQuery = "";

if ($numOfBandsLocationHas == 1) {
    $deleteQuery = "DELETE FROM locations WHERE id = :id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $locationId);
} else if ($numOfBandsLocationHas > 1) {
    $deleteQuery = "DELETE FROM locations_to_bands WHERE location_id = :id AND band_id = :band_id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $locationId);
    $deleteStmt->bindParam(':band_id', $bandId);
}

$deleteStmt->execute();

echo "OK";

// $venQuery = "SELECT COUNT(*) FROM venues WHERE location_id = :location_id";

// $venStmt = $conn->prepare($venQuery);
// $venStmt->bindParam(':location_id', $data->id);
// $venStmt->execute();

// $numOfVenues = $venStmt->fetchColumn();

// if ($numOfVenues > 0) {
//     echo "You have " . $numOfVenues . " venue(s) related to this location. Could not delete the location. Delete these venues first or set another location to these venues.";
//     return;
// }

// $query = "DELETE FROM locations WHERE id = :id";

// $stmt = $conn->prepare($query);
// $stmt->bindParam(':id', $data->id);
// $stmt->execute();

// unlink($img_location . $data->image);

// echo "OK";
