<?php

require_once '../headers.php';
require_once '../db.php';

$loc = json_decode(file_get_contents("php://input"));

if ($loc->id == null) {

    // Add Venue
    $locationQuery = "INSERT INTO locations
        SET
            name = :name,
            slug = :slug,
            image_file_name = :image";
            
    $locationStmt = $conn->prepare($locationQuery);

    $locationStmt->bindParam(':name', $loc->name);
    $locationStmt->bindParam(':slug', $loc->slug);
    $locationStmt->bindParam(':image', $loc->image);

    $locationStmt->execute();

    $newLocationId = $conn->lastInsertId();
} else {
    $newLocationId = $loc->id;
}

// Add Relation to the Band
$l2bQuery = "INSERT INTO locations_to_bands
    SET
        band_id = :band_id,
        location_id = :location_id";
        
$l2bStmt = $conn->prepare($l2bQuery);

$l2bStmt->bindParam(':band_id', $bandId);
$l2bStmt->bindParam(':location_id', $newLocationId);

$l2bStmt->execute();

$newl2bId = $conn->lastInsertId();

echo $newl2bId;
