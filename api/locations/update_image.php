<?php

require_once '../headers.php';
require_once '../db.php';

$loc = json_decode(file_get_contents("php://input"));

$query = "UPDATE locations
			SET
                image_file_name = :image
            WHERE id = :id";
        
$stmt = $conn->prepare($query);

$stmt->bindParam(':id', $loc->id);
$stmt->bindParam(':image', $loc->image);

$stmt->execute();
