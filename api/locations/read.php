<?php

require_once '../headers.php';
require_once '../db.php';

$byCurrentBand = isset($_GET['by_current_band']) ? $_GET['by_current_band'] === 'true' : false;

$query = "SELECT
	l.*,
	l2b.id AS l2b_id,
	(CASE
		WHEN name LIKE 'A %' THEN SUBSTR(name, 3)
		WHEN name LIKE 'An %' THEN SUBSTR(name, 4)
		WHEN name LIKE 'The %' THEN SUBSTR(name, 5)
		ELSE name END
	) AS sort_name,
	(SELECT COUNT(venues.id)
		FROM venues
		JOIN venues_to_bands AS v2b
			ON v2b.venue_id = venues.id
		WHERE venues.location_id = l.id AND v2b.band_id = :band_id) AS venues_num
	FROM locations AS l
	JOIN locations_to_bands AS l2b
		ON l2b.location_id = l.id ";

if ($byCurrentBand) {
	$query .= " WHERE l2b.band_id = :band_id ";
} else {
	$query .= " WHERE NOT EXISTS (
		SELECT *
		FROM locations_to_bands
		WHERE locations_to_bands.band_id = :band_id
		AND locations_to_bands.location_id = l.id) ";
}

$query .= " ORDER BY sort_name";


$stmt = $conn->prepare($query);
$stmt->bindParam(':band_id', $bandId);
$stmt->execute();

$arr = array();

while ($loc = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($loc);
    
	$item  = array(
		'id' => (int)$id,
		'l2bId' => (int)$l2b_id,
		'image' => $image_file_name,
		'name' => $name,
		'slug' => $slug,
		'venuesNum' => (int)$venues_num
	);

	array_push($arr, $item);
}

echo json_encode($arr);
