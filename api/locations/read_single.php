<?php

require_once '../headers.php';
require_once '../db.php';

$l2bId = isset($_GET['l2b_id']) ? $_GET['l2b_id'] : die();

$query = "SELECT
    l.*,
	l2b.id AS l2b_id,
	(CASE
		WHEN name LIKE 'A %' THEN SUBSTR(name, 3)
		WHEN name LIKE 'An %' THEN SUBSTR(name, 4)
		WHEN name LIKE 'The %' THEN SUBSTR(name, 5)
		ELSE name END
	) AS sort_name,
	(SELECT COUNT(venues.id)
		FROM venues
		JOIN venues_to_bands AS v2b
			ON v2b.venue_id = venues.id
		WHERE venues.location_id = l.id AND v2b.band_id = :band_id) AS venues_num
    FROM locations AS l
	JOIN locations_to_bands AS l2b
		ON l2b.location_id = l.id
    WHERE l2b.id = :l2b_id
    LIMIT 0,1";

$stmt = $conn->prepare($query);
$stmt->bindParam(':l2b_id', $l2bId);
$stmt->bindParam(':band_id', $bandId);
$stmt->execute();

$arr = array();

$loc = $stmt->fetch(PDO::FETCH_ASSOC);
extract($loc);

$item  = array(
    'id' => (int)$id,
	'l2bId' => (int)$l2b_id,
    'image' => $image_file_name,
    'name' => $name,
	'slug' => $slug,
	'venuesNum' => (int)$venues_num
);

echo json_encode($item);
