<?php

require_once '../headers.php';
require_once '../db.php';

$byCurrentBand = isset($_GET['by_current_band']) ? $_GET['by_current_band'] === 'true' : false;

$gigsQuery = "SELECT
        g.id AS gig_id,
        g2b.name,
        g2b.id AS g2b_id,
        g.date,
        g.venue_id,
        g2b.is_show_details,
        g2b.is_hide_manually,
        g2b.is_approx_setlist,
        v.name AS venue,
        v.slug AS venue_slug,
        l.image_file_name AS location_image,
        l.id AS location_id,
        l.name AS location_name,
        l.slug AS location_slug,
        (SELECT g2b.id
            FROM gigs AS g
            JOIN gigs_to_bands AS g2b
                ON g.id = g2b.gig_id
            WHERE g.venue_id = v.id
            AND g2b.band_id = :band_id
            ORDER BY g.date LIMIT 1) AS first_gig_on_venue_id,
        (SELECT g2b.id
            FROM gigs AS g
            JOIN venues
                ON venues.id = g.venue_id
            JOIN locations
                ON locations.id = venues.location_id
            JOIN gigs_to_bands AS g2b
                ON g.id = g2b.gig_id
            WHERE venues.location_id = l.id
            AND g2b.band_id = :band_id
            ORDER BY g.date LIMIT 1) AS first_gig_on_location_id
        FROM gigs AS g
        JOIN gigs_to_bands AS g2b
            ON g.id = g2b.gig_id
        JOIN venues AS v
            ON v.id = g.venue_id
        JOIN locations AS l
            ON l.id = v.location_id ";

if ($byCurrentBand) {
	$gigsQuery .= " WHERE g2b.band_id = :band_id ";
} else {
	$gigsQuery .= " WHERE NOT EXISTS (
		SELECT *
		FROM gigs_to_bands
		WHERE gigs_to_bands.band_id = :band_id
		AND gigs_to_bands.gig_id = g.id) ";
}

$gigsQuery .= " ORDER BY g.date";

$gigsStmt = $conn->prepare($gigsQuery);
$gigsStmt->bindParam(':band_id', $bandId);
$gigsStmt->execute();

$gigArr = array();

while ($gig = $gigsStmt->fetch(PDO::FETCH_ASSOC)) {
    extract($gig);

    // *** Get Songs
	$songArr = array();

    $songsQuery = "SELECT
            s.id AS song_id,
            s.name AS song_name,
            s2g.index AS song_index,
            s2g.is_in_braces,
            snv.variation,
            snv.id AS variation_id,
            s2g.id AS s2g_id,
            (SELECT g2b.id
                FROM gigs_to_bands AS g2b
                JOIN songs_to_g2b
                    ON g2b.id = songs_to_g2b.g2b_id
                JOIN gigs
                    ON gigs.id = g2b.gig_id
                JOIN songs
                    ON songs.id = songs_to_g2b.song_id
                WHERE g2b.band_id = :band_id
                AND songs_to_g2b.song_id = s.id
                ORDER BY gigs.date
                LIMIT 1) AS first_gig_id,
            (SELECT songs_to_g2b.id
                FROM songs_to_g2b
                WHERE songs_to_g2b.song_id = s.id
                AND songs_to_g2b.g2b_id = :g2b_id LIMIT 1) AS fist_time_on_same_gig_id
		FROM songs AS s
        JOIN songs_to_g2b AS s2g
            ON s2g.song_id = s.id
        LEFT OUTER JOIN song_name_variations AS snv
            ON snv.id = s2g.song_name_variation_id
        WHERE s2g.g2b_id = :g2b_id
        ORDER BY s2g.index";

	$songsStmt = $conn->prepare($songsQuery);
    $songsStmt->bindParam(':g2b_id', $g2b_id);
    $songsStmt->bindParam(':band_id', $bandId);
	$songsStmt->execute();

	while ($song = $songsStmt->fetch(PDO::FETCH_ASSOC)) {
		extract($song);

		$songItem  = array(
			'id' => (int)$song_id,
            'name' => $variation != null ? $is_in_braces ? $song_name : $variation : $song_name,
            'variationId' => $variation_id != null ? (int)$variation_id : null,
            'isInBraces' => $is_in_braces == 1,
            'braces' => $variation != null ? $is_in_braces ? $variation : $song_name : null,
            'index' => (int)$song_index,
            'isFirstTime' => $first_gig_id == $g2b_id && $s2g_id == $fist_time_on_same_gig_id,
            'delete' => false,
            'edit' => false,
            'added' => true
		);

		array_push($songArr, $songItem);
	}
    // *** End of Get Songs
    
    // *** Get Members
	$memberArr = array();

    $memQuery = "SELECT
            m.id AS mem_id,
            m.first_name,
            m.last_name,
            m2g.id AS m2g_id,
            m2b.id AS m2b_id,
            m2g.index AS mem_index,
            (SELECT g2b.id
                FROM gigs AS g
                JOIN gigs_to_bands AS g2b
                    ON g.id = g2b.gig_id
                JOIN m2b_to_g2b AS m2g
                    ON g2b.id = m2g.g2b_id
                WHERE m2g.m2b_id = m2b.id
                AND g2b.band_id = :band_id
                ORDER BY g.date LIMIT 1) AS first_gig_id
		FROM members AS m
        JOIN members_to_bands AS m2b
            ON m2b.member_id = m.id
        JOIN m2b_to_g2b AS m2g
            ON m2g.m2b_id = m2b.id
        WHERE m2g.g2b_id = :g2b_id
        ORDER BY m2g.index";

	$memStmt = $conn->prepare($memQuery);
	$memStmt->bindParam(':g2b_id', $g2b_id);
    $memStmt->bindParam(':band_id', $bandId);
	$memStmt->execute();

	while ($mem = $memStmt->fetch(PDO::FETCH_ASSOC)) {
        extract($mem);
        
        // *** Get Member's Roles on the Gig

        $roleArr = array();

        $rolesQuery = "SELECT
                r.id AS role_id,
                r.name AS role_name,
                r.slug AS role_slug,
                mr2g.index AS role_index
            FROM roles AS r
            JOIN member_roles_on_gigs AS mr2g
                ON mr2g.role_id = r.id
            JOIN m2b_to_g2b AS m2g
                ON m2g.id = mr2g.member_to_gigs_id
            WHERE m2g.m2b_id = :m2b_id
            AND m2g.g2b_id = :g2b_id";

        $rolesStmt = $conn->prepare($rolesQuery);
        $rolesStmt->bindParam(':g2b_id', $g2b_id);
        $rolesStmt->bindParam(':m2b_id', $m2b_id);
        $rolesStmt->execute();

        while ($role = $rolesStmt->fetch(PDO::FETCH_ASSOC)) {
            extract($role);

            $roleItem = array(
                'id' => (int)$role_id,
                'name' => $role_name,
                'slug' => $role_slug,
                'index' => (int)$role_index,
                'delete' => false,
                'edit' => false,
                'added' => true
            );

            array_push($roleArr, $roleItem);
        }
    
        // *** End of Get Member's Roles on the Gig

		$memberItem  = array(
            'id' => (int)$mem_id,
            'm2bId' => (int)$m2b_id,
            'm2gId' => (int)$m2g_id,
            'firstName' => $first_name,
            'lastName' => $last_name,
            'index' => (int)$mem_index,
            'isFirstTime' => $first_gig_id == $g2b_id,
            'delete' => false,
            'edit' => false,
            'added' => true
        );
        
        $memberItem['rolesOnGig'] = $roleArr;

		array_push($memberArr, $memberItem);
	}
	// *** End of Get Members
    
	$gigItem  = array(
        'id' => (int)$gig_id,
        'g2bId' => (int)$g2b_id,
        'name' => $name,
        'date' => $date,
        'venueId' => (int)$venue_id,
        'venue' => $venue,
        'venueSlug' => $venue_slug,
        'locationImage' => $location_image,
        'locationId' => $location_id,
        'locationName' => $location_name,
        'locationSlug' => $location_slug,
        'isVenueFirstTime' => $first_gig_on_venue_id == $g2b_id,
        'isLocationFirstTime' => $first_gig_on_location_id == $g2b_id,
        'isShowDetails' => $is_show_details == 1,
        'isShowDetailsLocal' => false,
		'isHideManually' => $is_hide_manually == 1,
		'isApproxSetlist' => $is_approx_setlist == 1
	);

    $gigItem['songs'] = $songArr;
    $gigItem['members'] = $memberArr;
    
	array_push($gigArr, $gigItem);
}

echo json_encode($gigArr);
