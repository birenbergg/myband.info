<?php

require_once '../headers.php';
require_once '../db.php';

$g2bId = isset($_GET['g2b_id']) ? $_GET['g2b_id'] : die();

$gigQuery = "SELECT
        g.id AS gig_id,
        g2b.name,
        g2b.id AS g2b_id,
        g.date,
        g.venue_id,
        g2b.is_show_details,
        g2b.is_hide_manually,
        g2b.is_approx_setlist,
        v.name AS venue,
        v.slug AS venue_slug,
        l.image_file_name AS location_image,
        l.id AS location_id,
        l.name AS location_name,
        l.slug AS location_slug,
        (SELECT g2b.id
            FROM gigs AS g
            JOIN gigs_to_bands AS g2b
                ON g.id = g2b.gig_id
            WHERE g.venue_id = v.id
            AND g2b.band_id = :band_id
            ORDER BY g.date LIMIT 1) AS first_gig_on_venue_id,
        (SELECT g2b.id
            FROM gigs AS g
            JOIN venues
                ON venues.id = g.venue_id
            JOIN locations
                ON locations.id = venues.location_id
            JOIN gigs_to_bands AS g2b
                ON g.id = g2b.gig_id
            WHERE venues.location_id = l.id
            AND g2b.band_id = :band_id
            ORDER BY g.date LIMIT 1) AS first_gig_on_location_id
        FROM gigs AS g
        JOIN gigs_to_bands AS g2b
            ON g.id = g2b.gig_id
        JOIN venues AS v
            ON v.id = g.venue_id
        JOIN locations AS l
            ON l.id = v.location_id
        WHERE g2b.id = :g2b_id
        LIMIT 0,1";

$gigStmt = $conn->prepare($gigQuery);
$gigStmt->bindParam(':g2b_id', $g2bId);
$gigStmt->bindParam(':band_id', $bandId);
$gigStmt->execute();

$gig = $gigStmt->fetch(PDO::FETCH_ASSOC);
extract($gig);

// *** Get Songs
$songArr = array();

$songQuery = "SELECT
    s.id AS song_id,
    s.name AS song_name,
    s2g.index AS song_index,
    s2g.is_in_braces,
    snv.variation,
    snv.id AS variation_id,
    s2g.id AS s2g_id,
    (SELECT g2b.id
        FROM gigs_to_bands AS g2b
        JOIN songs_to_g2b
            ON g2b.id = songs_to_g2b.g2b_id
        JOIN gigs
            ON gigs.id = g2b.gig_id
        JOIN songs
            ON songs.id = songs_to_g2b.song_id
        WHERE g2b.band_id = :band_id
        AND songs_to_g2b.song_id = s.id
        ORDER BY gigs.date
        LIMIT 1) AS first_gig_id,
    (SELECT songs_to_g2b.id
        FROM songs_to_g2b
        WHERE songs_to_g2b.song_id = s.id
        AND songs_to_g2b.g2b_id = :g2b_id limit 1) AS fist_time_on_same_gig_id
    FROM songs AS s
    JOIN songs_to_g2b AS s2g
        ON s2g.song_id = s.id
    LEFT OUTER JOIN song_name_variations AS snv
        ON snv.id = s2g.song_name_variation_id
    WHERE s2g.g2b_id = :g2b_id
    ORDER BY s2g.index";

$songStmt = $conn->prepare($songQuery);
$songStmt->bindParam(':g2b_id', $g2bId);
$songStmt->bindParam(':band_id', $bandId);
$songStmt->execute();

while ($song = $songStmt->fetch(PDO::FETCH_ASSOC)) {
    extract($song);

    $songItem  = array(
        'id' => (int)$song_id,
        'name' => $variation != null ? $is_in_braces ? $song_name : $variation : $song_name,
        'variationId' => $variation_id != null ? (int)$variation_id : null,
        'isInBraces' => $is_in_braces == 1,
        'braces' => $variation != null ? $is_in_braces ? $variation : $song_name : null,
        'index' => (int)$song_index,
        'isFirstTime' => $first_gig_id == $g2bId && $s2g_id == $fist_time_on_same_gig_id,
        'delete' => false,
        'edit' => false,
        'added' => true
    );

    array_push($songArr, $songItem);
}
// *** End of Get Songs

// *** Get Members
$memberArr = array();

$memQuery = "SELECT
        m.id AS mem_id,
        m.first_name,
        m.last_name,
        m2g.id AS m2g_id,
        m2b.id AS m2b_id,
        m2g.index AS mem_index,
        (SELECT g2b.id
        FROM gigs AS g
            JOIN gigs_to_bands AS g2b
                ON g.id = g2b.gig_id
            JOIN m2b_to_g2b AS m2g
                ON g2b.id = m2g.g2b_id
            WHERE m2g.m2b_id = m2b.id
            AND g2b.band_id = :band_id
            ORDER BY g.date LIMIT 1) AS first_gig_id
    FROM members AS m
    JOIN members_to_bands AS m2b
        ON m2b.member_id = m.id
    JOIN m2b_to_g2b AS m2g
        ON m2g.m2b_id = m2b.id
    WHERE m2g.g2b_id = :g2b_id
    ORDER BY m2g.index";

$memStmt = $conn->prepare($memQuery);
$memStmt->bindParam(':g2b_id', $g2bId);
$memStmt->bindParam(':band_id', $bandId);
$memStmt->execute();

while ($mem = $memStmt->fetch(PDO::FETCH_ASSOC)) {
    extract($mem);
    
    // *** Get Member's Roles on the Gig

    $roleArr = array();

    $roleQuery = "SELECT
            r.id AS role_id,
            r.name AS role_name,
            r.slug AS role_slug,
            mr2g.index AS role_index
        FROM roles AS r
        JOIN member_roles_on_gigs AS mr2g
            ON mr2g.role_id = r.id
        JOIN m2b_to_g2b AS m2g
            ON m2g.id = mr2g.member_to_gigs_id
        WHERE m2g.m2b_id = :m2b_id
        AND m2g.g2b_id = :g2b_id";

    $roleStmt = $conn->prepare($roleQuery);
    $roleStmt->bindParam(':g2b_id', $g2bId);
    $roleStmt->bindParam(':m2b_id', $m2b_id);
    $roleStmt->execute();

    while ($role = $roleStmt->fetch(PDO::FETCH_ASSOC)) {
        extract($role);
        
        $roleItem = array(
            'id' => (int)$role_id,
            'name' => $role_name,
            'slug' => $role_slug,
            'index' => (int)$role_index,
            'delete' => false,
            'edit' => false,
            'added' => true
        );

        array_push($roleArr, $roleItem);
    }

    // *** End of Get Member's Roles on the Gig

    $memberItem  = array(
        'id' => (int)$mem_id,
        'm2bId' => (int)$m2b_id,
        'm2gId' => (int)$m2g_id,
        'firstName' => $first_name,
        'lastName' => $last_name,
        'index' => (int)$mem_index,
        'isFirstTime' => $first_gig_id == $g2bId,
        'delete' => false,
        'edit' => false,
        'added' => true
    );
    
    $memberItem['rolesOnGig'] = $roleArr;

    array_push($memberArr, $memberItem);
}
// *** End of Get Members

$gigItem  = array(
    'id' => (int)$gig_id,
    'g2bId' => (int)$g2bId,
    'name' => $name,
    'date' => $date,
    'venue' => $venue,
    'venueSlug' => $venue_slug,
    'venueId' => (int)$venue_id,
    'locationImage' => $location_image,
    'locationId' => $location_id,
    'locationName' => $location_name,
    'locationSlug' => $location_slug,
    'isVenueFirstTime' => $first_gig_on_venue_id == $g2bId,
    'isLocationFirstTime' => $first_gig_on_location_id == $g2bId,
    'isShowDetails' => $is_show_details == 1,
    'isShowDetailsLocal' => false,
    'isHideManually' => $is_hide_manually == 1,
    'isApproxSetlist' => $is_approx_setlist == 1
);

$gigItem['songs'] = $songArr;
$gigItem['members'] = $memberArr;

echo json_encode($gigItem);
