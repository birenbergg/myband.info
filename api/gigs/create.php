<?php

require_once '../headers.php';
require_once '../db.php';

$gig = json_decode(file_get_contents("php://input"));

if ($gig->id == null) {
    // Add Gig
    $gigQuery = "INSERT INTO gigs
        SET
            date = :date,
            venue_id = :venue_id";
            
    $gigStmt = $conn->prepare($gigQuery);

    $gigStmt->bindParam(':date', $gig->date);
    $gigStmt->bindParam(':venue_id', $gig->venueId);

    $gigStmt->execute();

    $newGigId = $conn->lastInsertId();
} else {
    $newGigId = $gig->id;
}

// Add Relation to the Band
$g2bQuery = "INSERT INTO gigs_to_bands
    SET
        band_id = :band_id,
        gig_id = :gig_id,
        `name` = :name,
        is_show_details = :is_show_details,
        is_hide_manually = :is_hide_manually,
        is_approx_setlist = :is_approx_setlist";
        
$g2bStmt = $conn->prepare($g2bQuery);

$g2bStmt->bindParam(':band_id', $bandId);
$g2bStmt->bindParam(':gig_id', $newGigId);
$g2bStmt->bindParam(':name', $gig->name);
$g2bStmt->bindParam(':is_show_details', $isShowDetails = $gig->isShowDetails ? 1 : 0);
$g2bStmt->bindParam(':is_hide_manually', $isHideManually = $gig->isHideManually ? 1 : 0);
$g2bStmt->bindParam(':is_approx_setlist', $isApproxSetlist = $gig->isApproxSetlist ? 1 : 0);

$g2bStmt->execute();

$g2bId = $conn->lastInsertId();

// Add Songs
if ($gig->songs != null) {
    $songIndex = 1;
    foreach ($gig->songs as $song) {
        if (!$song->delete) {
            $songQuery = "INSERT INTO songs_to_g2b
                SET
                    g2b_id = :g2b_id,
                    song_id = :song_id,
                    `index` = :index,
                    song_name_variation_id = :song_name_variation_id,
                    is_in_braces = :is_in_braces";
            
            $songStmt = $conn->prepare($songQuery);
    
            $songStmt->bindParam(':g2b_id', $g2bId);
            $songStmt->bindParam(':song_id', $song->id);
            $songStmt->bindParam(':index', $songIndex);
            $songStmt->bindParam(':song_name_variation_id', $song->variationId);
            $songStmt->bindParam(':is_in_braces', $isInBraces = $song->isInBraces ? 1 : 0);
            
            $songStmt->execute();
            
            $songIndex++;
        }
    }
}
// End: Add Songs

// Add Members
if ($gig->members != null) {
    $memberIndex = 1;
    foreach ($gig->members as $member) {
        if (!$member->delete) {
            $memberQuery = "INSERT INTO m2b_to_g2b
                SET
                    g2b_id = :g2b_id,
                    m2b_id = :m2b_id,
                    `index` = :index";
            
            $memberStmt = $conn->prepare($memberQuery);

            $memberStmt->bindParam(':g2b_id', $g2bId);
            $memberStmt->bindParam(':m2b_id', $member->m2bId);
            $memberStmt->bindParam(':index', $memberIndex);

            $memberStmt->execute();

            $memberIndex++;

            $memberToGigId = $conn->lastInsertId();

            // Add Memeber Roles
            if ($member->rolesOnGig != null) {
                $roleIndex = 1;
                foreach ($member->rolesOnGig as $role) {
                    if (!$role->delete) {
                        $roleQuery = "INSERT INTO member_roles_on_gigs
                            SET
                                member_to_gigs_id = :member_to_gigs_id,
                                role_id = :role_id,
                                `index` = :index";
                        
                        $roleStmt = $conn->prepare($roleQuery);
                
                        $roleStmt->bindParam(':member_to_gigs_id', $memberToGigId);
                        $roleStmt->bindParam(':role_id', $role->id);
                        $roleStmt->bindParam(':index', $roleIndex);
                
                        $roleStmt->execute();
                        
                        $roleIndex++;
                    }
                }
            }
        }
        // End: Add Memeber Roles
    }
}
// End: Add Members

echo $g2bId;
