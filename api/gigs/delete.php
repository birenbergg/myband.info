<?php

require_once '../headers.php';
require_once '../db.php';

$gigId = json_decode(file_get_contents("php://input"));

$checkQuery = "SELECT COUNT(*)
	FROM  gigs_to_bands
    WHERE gig_id = :gig_id";

$checkStmt = $conn->prepare($checkQuery);
$checkStmt->bindParam(':gig_id', $gigId);
$checkStmt->execute();

$numOfBandsGigHas = (int)$checkStmt->fetchColumn();

$deleteQuery = "";

if ($numOfBandsGigHas == 1) {
    $deleteQuery = "DELETE FROM gigs WHERE id = :id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $gigId);
} else if ($numOfBandsGigHas > 1) {
    $deleteQuery = "DELETE FROM gigs_to_bands WHERE gig_id = :id AND band_id = :band_id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $gigId);
    $deleteStmt->bindParam(':band_id', $bandId);
}

$deleteStmt->execute();

echo "OK";
