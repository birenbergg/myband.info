<?php

require_once '../headers.php';
require_once '../db.php';
require_once '../identify_band.php'; // DELETE?

$list = json_decode(file_get_contents("php://input"));

$query = "INSERT INTO lists
    SET
        name = :name,
        band_id = :band_id";
        
$stmt = $conn->prepare($query);

$stmt->bindParam(':name', $list->name);
$stmt->bindParam(':band_id', $bandId);

$stmt->execute();

$newListId = $conn->lastInsertId();

// Add Songs
if ($list->songs != null) {
    $songIndex = 1;
    foreach ($list->songs as $song) {
        if (!$song->delete) {
            $songQuery = "INSERT INTO songs_to_lists
                SET
                    list_id = :list_id,
                    song_id = :song_id,
                    `index` = :index";
            
            $songStmt = $conn->prepare($songQuery);
    
            $songStmt->bindParam(':list_id', $newListId);
            $songStmt->bindParam(':song_id', $song->id);
            $songStmt->bindParam(':index', $songIndex);
            
            $songStmt->execute();
            
            $songIndex++;
        }
    }
}
// End: Add Songs

echo $newListId;
