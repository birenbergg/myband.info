<?php

require_once '../headers.php';
require_once '../db.php';

$list = json_decode(file_get_contents("php://input"));

$query = "UPDATE lists
    SET
        name = :name
    WHERE id = :id";
        
$stmt = $conn->prepare($query);

$stmt->bindParam(':id', $list->id);
$stmt->bindParam(':name', $list->name);

$stmt->execute();

// Update Songs
// (first delete all songs)
$deleteSongsQuery = "DELETE FROM songs_to_lists
    WHERE
        list_id = :list_id";
            
$deleteSongsStmt = $conn->prepare($deleteSongsQuery);

$deleteSongsStmt->bindParam(':list_id', $list->id);

$deleteSongsStmt->execute();

// then add the songs again
if ($list->songs != null) {
    $songIndex = 1;
    foreach ($list->songs as $song) {
        if (!$song->delete) {
            $songQuery = "INSERT INTO songs_to_lists
                SET
                    list_id = :list_id,
                    song_id = :song_id,
                    `index` = :index";
            
            $songStmt = $conn->prepare($songQuery);
    
            $songStmt->bindParam(':list_id', $list->id);
            $songStmt->bindParam(':song_id', $song->id);
            $songStmt->bindParam(':index', $songIndex);
            
            $songStmt->execute();
            
            $songIndex++;
        }
    }
}
// End: Update Songs
