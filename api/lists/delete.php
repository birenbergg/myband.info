<?php

require_once '../headers.php';
require_once '../db.php';

$data = json_decode(file_get_contents("php://input"));

$query = "DELETE FROM lists WHERE id = :id";

$stmt = $conn->prepare($query);
$stmt->bindParam(':id', $data->id);
$stmt->execute();

echo "OK";
