<?php

require_once '../headers.php';
require_once '../db.php';

$id = isset($_GET['id']) ? $_GET['id'] : die();

$query = "SELECT
    l.id AS list_id,
    l.name AS list_name
FROM lists AS l
WHERE l.id = :id
LIMIT 0,1";

$stmt = $conn->prepare($query);
$stmt->bindParam(':id', $id);
$stmt->execute();

$list = $stmt->fetch(PDO::FETCH_ASSOC);
extract($list);

// *** Get Songs
$songArr = array();

$songQuery = "SELECT
        s.id AS song_id,
        s.name AS song_name,
	    s.length,
        s2l.index AS song_index
    FROM songs AS s
    JOIN songs_to_lists AS s2l
        ON s2l.song_id = s.id
    WHERE s2l.list_id = :list_id";

$songStmt = $conn->prepare($songQuery);
$songStmt->bindParam(':list_id', $list_id);
$songStmt->execute();

while ($song = $songStmt->fetch(PDO::FETCH_ASSOC)) {
    extract($song);

    $songItem  = array(
        'id' => (int)$song_id,
        'name' => $song_name,
		'totalLength' => $length != null ? (int)$length : null,
		'minutes' => floor($length / 60),
		'seconds' => $length - floor($length / 60) * 60,
        'index' => (int)$song_index,
        'delete' => false,
        'edit' => false,
        'added' => true
    );

    array_push($songArr, $songItem);
}
// *** End of Get Songs

$item  = array(
    'id' => (int)$list_id,
    'name' => $list_name
);

$item['songs'] = $songArr;

echo json_encode($item);
