<?php

require_once '../headers.php';
require_once '../db.php';

$query = "SELECT * FROM band_names AS bn
        WHERE bn.band_id = :band_id";

$stmt = $conn->prepare($query);
$stmt->bindParam(':band_id', $bandId);
$stmt->execute();

$bnArr = array();

while ($bn = $stmt->fetch(PDO::FETCH_ASSOC)) {
	extract($bn);

	$bnItem  = array(
		'name' => $name,
		'dateStarted' => $date_started
    );

	array_push($bnArr, $bnItem);
}

echo json_encode($bnArr);
