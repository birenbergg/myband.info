<?php

require_once '../headers.php';
require_once '../db.php';

$bandQuery = "SELECT * FROM bands AS b ORDER BY b.name";

$bandStmt = $conn->prepare($bandQuery);
$bandStmt->execute();

$bandArr = array();

while ($band = $bandStmt->fetch(PDO::FETCH_ASSOC)) {
	extract($band);

	$bandItem  = array(
		'id' => (int)$id,
		'slug' => $slug,
		'name' => $name,
		'creationDate' => $creation_date,
		'dismissalDate' => $dismissal_date,
		'imageName' => $image_name
    );

	array_push($bandArr, $bandItem);
}

echo json_encode($bandArr);
