<?php

require_once '../headers.php';
require_once '../db.php';

$id = isset($_GET['id']) ? $_GET['id'] : die();

$query = "SELECT * FROM bands AS b
        WHERE b.id = :id
        LIMIT 0,1";

$stmt = $conn->prepare($query);
$stmt->bindParam(':id', $id);
$stmt->execute();

$band = $stmt->fetch(PDO::FETCH_ASSOC);
extract($band);

$item  = array(
    'id' => (int)$id,
    'slug' => $slug,
    'name' => $name,
    'email' => $email,
    'creationDate' => $creation_date,
    'dismissalDate' => $dismissal_date,
    'imageName' => $image_name
);

echo json_encode($item);
