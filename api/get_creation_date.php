<?php

require_once 'headers.php';
require_once 'db.php';

$query = "SELECT b.creation_date FROM bands AS b WHERE b.id = :band_id";

$stmt = $conn->prepare($query);
$stmt->bindParam(':band_id', $bandId);
$stmt->execute();

$arr = array();

$band = $stmt->fetch(PDO::FETCH_ASSOC);
extract($band);

echo json_encode($band);
