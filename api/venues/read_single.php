<?php

require_once '../headers.php';
require_once '../db.php';

$ven_id = isset($_GET['id']) ? $_GET['id'] : die();

$query = "SELECT
        v.id,
        v.name,
        v.slug,
        (CASE
			WHEN v.name LIKE 'A %' THEN SUBSTR(v.name, 3)
			WHEN v.name LIKE 'An %' THEN SUBSTR(v.name, 4)
			WHEN v.name LIKE 'The %' THEN SUBSTR(v.name, 5)
			ELSE v.name END
		) AS sort_name,
		v.contact,
		v.phone,
		v.url,
		v.notes,
        v.location_id,
        l.name AS location,
		l.image_file_name AS location_image,
		COUNT(g2b.id) AS gigs_num,
		COUNT(IF(g2b.gig_id = g.id AND g.date < CURDATE(), 1, NULL)) AS past_gigs_num
    FROM venues AS v
    JOIN locations AS l
        ON v.location_id = l.id
	LEFT JOIN gigs AS g
		ON v.id = g.venue_id
    LEFT JOIN gigs_to_bands AS g2b
		ON g2b.gig_id = g.id AND g2b.band_id = :band_id
    WHERE v.id = :ven_id
    GROUP BY v.id
    LIMIT 0,1";

$stmt = $conn->prepare($query);
$stmt->bindParam(':ven_id', $ven_id);
$stmt->bindParam(':band_id', $bandId);
$stmt->execute();

$arr = array();

$ven = $stmt->fetch(PDO::FETCH_ASSOC);
extract($ven);

$item  = array(
    'id' => (int)$id,
    'name' => $name,
		'slug' => $slug,
    'sortName' => $sort_name,
    'contact' => $contact,
    'phone' => $phone,
    'url' => $url,
    'notes' => $notes,
    'locationId' => (int)$location_id,
    'location' => $location,
    'locationImage' => $location_image,
    'gigsNum' => (int)$gigs_num,
    'pastGigsNum' => (int)$past_gigs_num
);

echo json_encode($item);
