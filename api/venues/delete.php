<?php

require_once '../headers.php';
require_once '../db.php';

$venueId = json_decode(file_get_contents("php://input"));

$checkQuery = "SELECT COUNT(*)
	FROM  venues_to_bands
    WHERE venue_id = :venue_id";

$checkStmt = $conn->prepare($checkQuery);
$checkStmt->bindParam(':venue_id', $venueId);
$checkStmt->execute();

$numOfBandsVenueHas = (int)$checkStmt->fetchColumn();

$deleteQuery = "";

if ($numOfBandsVenueHas == 1) {
    $deleteQuery = "DELETE FROM venues WHERE id = :id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $venueId);
} else if ($numOfBandsVenueHas > 1) {
    $deleteQuery = "DELETE FROM venues_to_bands WHERE venue_id = :id AND band_id = :band_id";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bindParam(':id', $venueId);
    $deleteStmt->bindParam(':band_id', $bandId);
}

$deleteStmt->execute();

echo "OK";
