<?php

require_once '../headers.php';
require_once '../db.php';

$ven = json_decode(file_get_contents("php://input"));

if ($ven->id == null) {

    // Add Venue
    $venueQuery = "INSERT INTO venues
        SET
            name = :name,
            slug = :slug,
            contact = :contact,
            phone = :phone,
            url = :url,
            notes = :notes,
            location_id = :location_id";
            
    $venueStmt = $conn->prepare($venueQuery);

    $venueStmt->bindParam(':name', $ven->name);
    $venueStmt->bindParam(':slug', $ven->slug);
    $venueStmt->bindParam(':contact', $ven->contact);
    $venueStmt->bindParam(':phone', $ven->phone);
    $venueStmt->bindParam(':url', $ven->url);
    $venueStmt->bindParam(':notes', $ven->notes);
    $venueStmt->bindParam(':location_id', $locationId = (int)$ven->locationId);

    $venueStmt->execute();

    $newVenueId = $conn->lastInsertId();
} else {
    $newVenueId = $ven->id;
}

// Add Relation to the Band
$v2bQuery = "INSERT INTO venues_to_bands
    SET
        band_id = :band_id,
        venue_id = :venue_id";
        
$v2bStmt = $conn->prepare($v2bQuery);

$v2bStmt->bindParam(':band_id', $bandId);
$v2bStmt->bindParam(':venue_id', $newVenueId);

$v2bStmt->execute();

echo $newVenueId;
