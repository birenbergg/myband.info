<?php

require_once '../headers.php';
require_once '../db.php';

$ven = json_decode(file_get_contents("php://input"));

$query = "UPDATE venues
			SET
                name = :name,
                slug = :slug,
                contact = :contact,
                phone = :phone,
                url = :url,
                notes = :notes,
                location_id = :location_id
            WHERE id = :id";
        
$stmt = $conn->prepare($query);

$stmt->bindParam(':id', $ven->id);
$stmt->bindParam(':name', $ven->name);
$stmt->bindParam(':slug', $ven->slug);
$stmt->bindParam(':contact', $ven->contact);
$stmt->bindParam(':phone', $ven->phone);
$stmt->bindParam(':url', $ven->url);
$stmt->bindParam(':notes', $ven->notes);
$stmt->bindParam(':location_id', $ven->locationId);

$stmt->execute();
