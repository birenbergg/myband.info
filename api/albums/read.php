<?php

require_once '../headers.php';
require_once '../db.php';

$byCurrentBand = isset($_GET['by_current_band']) ? $_GET['by_current_band'] === 'true' : false;

$query = "SELECT
        *
    FROM albums AS a ";

if ($byCurrentBand) {
	$query .= " WHERE band_id = :band_id ";
}

$stmt = $conn->prepare($query);

if ($byCurrentBand) {
    $stmt->bindParam(':band_id', $bandId);
}

$stmt->execute();

$arr = array();

while ($album = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($album);
    
	$item  = array(
		'id' => (int)$gig_id,
        'name' => $name,
        'date' => $date,
        'slug' => $slug
	);
    
	array_push($arr, $item);
}

echo json_encode($arr);
